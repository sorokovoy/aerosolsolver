#include "XiSolverTestEngine.h"

#include <stdio.h>
#include <math.h>
#include <time.h>

#define AS_Pi 3.14159265358979323846
#define ASMax(a,b)    (((a) > (b)) ? (a) : (b))
#define ASMin(a,b)    (((a) < (b)) ? (a) : (b))



#define GUTimerInit(TimerName)   clock_t TimerName = clock();
#define GUTimerReset(TimerName)  TimerName = clock();
#define GUTimerGetDiffInSec(TimerName)  ((double)(clock() - TimerName) / CLOCKS_PER_SEC)

#include "XiQSSASolver.h"
#include "XiQSSA2OrderSolver.h"
#include "XiSimpleSolver.h"
#include "XiSimple2OrderSolver.h"
#include "XiSolverTestEngine.h"
#include "XiSolverExactSolutions.h"
#include "XiSolverSmoluhovskiExactSolutions.h"


void XiSolverTestEngine::RunTest(IXiSolver* ipSolver, IXiSolverTestEnginerExactSolution* iTarget)
{
  unsigned int nRad = 10000;

  XiSolverInput& input = *(new XiSolverInput());

  input.RadiusCount = nRad;
  input.TimeStep = 0.1;
  input.TimeStepCount = 4;
  input.pRadius = new double[nRad];
  input.pInitialDistribution = new double[nRad];
  input.pEqualationData = iTarget->CreateEqualationData();

  input.pEqualationData->pProduction->pMasterData = &input;
  input.pEqualationData->pDestruction->pMasterData = &input;

  input.RadiusStep = 0.001;
  for(unsigned int i = 0; i < nRad; i++) {
    input.pRadius[i] = (1.00 / nRad) * (i + 1);
    input.pInitialDistribution[i] = iTarget->Boundary(input.pRadius[i]);
  }

  for(int i = -7; i < 3; i++) {
    input.TimeStep = pow(10.0, i);

    GUTimerInit(calc);
    XiSolverOutput* pOut = ipSolver->Solve(&input);
    printf("%F \n ", GUTimerGetDiffInSec(calc));

    double norm1 = 0;
    double norm  = 0;

    double norm2  = 0;

    double maxValue  = 0;
    double maxDIffABS = 0;

    for(unsigned int t =  pOut->pInput->TimeStepCount - 1; t < pOut->pInput->TimeStepCount; t++) {
      double maxDIff = 0;
      unsigned int idxMaxDiff = 0;

      double layerNormDiff = 0;
      double layerNormValue = 0;

      
      double layerNormDiff2 = 0;
      for(unsigned int r = 0; r < pOut->pInput->RadiusCount; r++) {
        double sVal = iTarget->Solution(pOut->pInput->pRadius[r], pOut->times[t]);
        double tVal = pOut->pFinalDistribution[t][r];
        layerNormDiff += (sVal - tVal) * (sVal - tVal);
        layerNormValue += sVal * sVal;

        //layerNormDiff2 +=  (pOut->pFinalDistribution[t][r] - pOut2->pFinalDistribution[t][r]) * (pOut->pFinalDistribution[t][r] - pOut2->pFinalDistribution[t][r]);

        norm  += fabs(tVal - sVal);
        norm1 += fabs(sVal);
        norm2 += fabs((tVal - sVal) / ASMax(sVal, 1.00E-20));
        maxDIffABS = ASMax(maxDIffABS, fabs(pOut->pFinalDistribution[t][r] - sVal));
        //printf(" < %E %E > ", pOut->pFinalDistribution[t][r] , sVal);
      }
      //printf("\n");
      //printf("%E %d %E %d\n", maxDIff, idxMaxDiff,  maxDIffABS, 0);
      if( t == pOut->pInput->TimeStepCount - 1) {
        printf("%E %E %E\n", (layerNormDiff), (layerNormDiff) / (layerNormValue), layerNormDiff2);
      }
    }
    printf("ST %E -> N1 10^%d N2 10^%d PER_VAL 10^%d DIFF %E\n", input.TimeStep, (int)(log10((norm / norm1))), (int)(log10((norm2 / norm1))), (int)(log10((norm / (pOut->pInput->TimeStepCount * pOut->pInput->RadiusCount)))), maxDIffABS);
    delete pOut;
  }
  delete ipSolver;
  delete iTarget;

  printf("FINISH \n");
}
