#ifndef GeneralCoreEquation_H
#define GeneralCoreEquation_H

#include "XiSolverTestEngine.h"

class SmoluhovskiGeneralCore : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};

double E(double Ri, double Rk); //koefficient of effective of couple
double Kn(double Ri);
double Cn(double Ri);
double Us(double Ri);
double Kg(double Ri, double Rk);
double B(double Ri);
double Kb(double Ri, double Rk);
double alphaD(double Ri);
double alphaS(double Ri);

double Kni(double Ri);
double Di(double Ri);
double f(double Ri);
double U(double Ri);
double Vd(double Ri);

#endif