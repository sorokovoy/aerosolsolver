#include "SmoluhovskiKoreElevator.h"

#define AS_Pi 3.14159265358979323846
double SmoluhovskiCoreAdditive_Solution1(double x, double t)
{
  double v = pow(x, -1.50) * exp(-t) * exp(-exp(-2*t) * x / 2);
  v = v/(sqrt( 2 * AS_Pi));
  return v;
}

double SmoluhovskiCoreAdditive_SolutionDer1(double x, double t)
{
  return SmoluhovskiCoreAdditive_Solution1(x, t) * (exp(-2*t) * x - 1);
}


double* SmoluhovskiKoreElevator::Evaluate(std::vector<ISmoluhovskiKoreFunc*> iBasis, std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDIff , NValue iVals)
//double* SmoluhovskiKoreElevator::Evaluate(std::vector<ISmoluhovskiKoreFunc*> iBasis, NValue iVals)
{
  unsigned int size = iBasis.size() + iBasisPer.size() + iBasisDIff.size();
  for (unsigned int i = 0; i < size; i++) {
    std::vector<FBFixedSizeAverageCalculator<double>*> curr;
    for (unsigned int j = 0; j < size + 1; j++) {
      curr.push_back(new FBFixedSizeAverageCalculator<double>(10000));
    }
    _matrixSource.push_back(curr);
  }

  _correlationMatrix = new Eigen::MatrixXd(size, size);
  _correlationRightPart = new Eigen::VectorXd(size);

  _usedBasis = iBasis;


  for(unsigned int t = 2; t < iVals.tCount; t++) {
    for(unsigned int x = 30; x < iVals.xCount; x++) {

      double dt = (iVals.pVals[t][x] - iVals.pVals[t-1][x]) / iVals.tStep;

     // dt = SmoluhovskiCoreAdditive_SolutionDer1(iVals.pRads[x], t * iVals.tStep);

      double* pCoefs = new double[size];


      unsigned int pc = 0;

      for(unsigned int bc = 0; bc < iBasis.size(); bc++) {
        pCoefs[bc] = 0;

        unsigned int n = iVals.xCount;
        double prod = 0;
        for(unsigned int i = 0; i < x; i++) {
          prod += iVals.pVals[t][x - i] * iVals.pVals[t][i] * iBasis[bc]->K(iVals.pRads[x - i], iVals.pRads[i]);
        }
        prod = prod/2;
        prod = prod * iVals.xStep;

        double des = 0;
        for(unsigned int i = 0; i < n; i++) {
          des += iVals.pVals[t][i] * iBasis[bc]->K(iVals.pRads[i], iVals.pRads[x]); // pMasterData->pRadius[i] * pMasterData->pRadius[iCurrentIDx];
        }
        des*= iVals.pVals[t][x];
        des = des * iVals.xStep;

        pCoefs[pc] = prod - des;
        pc++;
      }

      for(unsigned int bc = 0; bc < iBasisPer.size(); bc++) {
        pCoefs[pc] = 0;
        unsigned int n = iVals.xCount;
        if(x != 0) {
          pCoefs[pc] = iBasisPer[bc]->F(iVals.pRads[x]) * (iVals.pVals[t][x - 1] - iVals.pVals[t][x]);// /  iVals.xStep;
        }
        pc++;
      }

      for(unsigned int bc = 0; bc < iBasisDIff.size(); bc++) {
        pCoefs[pc] = 0;
        unsigned int n = iVals.xCount;
        if(x != iVals.xCount - 1 && x != 0) {
          pCoefs[pc] = iBasisDIff[bc]->F(iVals.pRads[x]) * (iVals.pVals[t][x + 1] - 2 * iVals.pVals[t][x] + iVals.pVals[t][x - 1]);// / (iVals.xStep * iVals.xStep);
        }
        pc++;
      }

      ProcessDataRow(pCoefs, dt, size);
      delete pCoefs;
    }
  }
  return Evaluate(size);
}

void SmoluhovskiKoreElevator::ProcessDataRow(double* ipData, double v, unsigned int size)
{
  unsigned int n = size;
  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++) {
      _matrixSource[i][j]->AddElement(ipData[i] * ipData[j]);
    }
  }

  for(int i = 0; i < n; i++) {
    _matrixSource[i][n]->AddElement(ipData[i] * v);
  }
}

#include "Eigen/LU"
#include "Eigen/SVD"

double* SmoluhovskiKoreElevator::Evaluate(unsigned int size)
{
  unsigned int n =  size;
  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++) {
      (*_correlationMatrix)(i, j) = _matrixSource[i][j]->GetAverage();
    }
  }

  for(int i = 0; i < n; i++) {
    (*_correlationRightPart)(i) = _matrixSource[i][n]->GetAverage();
  }

   Eigen::JacobiSVD<Eigen::MatrixXd> svd(*_correlationMatrix, Eigen::ComputeThinU | Eigen::ComputeThinV);
  _solution = svd.solve(*_correlationRightPart);

  double* res = new double [n];
  for(unsigned int i = 0; i < n; i++) {
    res[i] = _solution[i];
  }

  return res;

}
