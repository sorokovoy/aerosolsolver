#ifndef BackTestRunnner_H
#define BackTestRunnner_H

#include "IXiSolver.h"
#include "SmoluhovskiKoreElevator.h"

class IDRRGBColorGradientInterpretator;

void RunCoreElevationBackTest(NValue* ipData, IDRRGBColorGradientInterpretator* ipColor, std::vector<ISmoluhovskiKoreFunc*> ipbasis, std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDiff, IXiSolver* ipSolver, const char* ipOutFile);
//void RunCoreElevationBackTest(NValue* ipData, IDRRGBColorGradientInterpretator* ipColor, std::vector<ISmoluhovskiKoreFunc*> ipbasis, IXiSolver* ipSolver, const char* ipOutFile);

#endif
