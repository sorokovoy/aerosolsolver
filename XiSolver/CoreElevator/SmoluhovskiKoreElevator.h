#ifndef SmoluhovskiKoreElevator_H
#define SmoluhovskiKoreElevator_H

#include "FBFixedSizeAverageCalculator.h"

// using linear algebra engine
#include "Eigen/Core"

class ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const = 0;
};

class IUnaryFunc
{
public:
  virtual double F(double x) const = 0;
};

struct NValue
{
  double** pVals;
  double* pRads;
  unsigned int tCount;
  unsigned int xCount;
  double tStep;
  double xStep;
};

#include <vector>

class SmoluhovskiKoreElevator
{
public:
  double* Evaluate(std::vector<ISmoluhovskiKoreFunc*> iBasis, std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDIff , NValue iVals);

private:
  NValue _input;
  void ProcessDataRow(double* ipData, double v, unsigned int size);
  double* Evaluate(unsigned int size);


  std::vector<ISmoluhovskiKoreFunc*> _usedBasis;

  Eigen::MatrixXd* _correlationMatrix;
  Eigen::VectorXd* _correlationRightPart;
  Eigen::VectorXd _solution;
  Eigen::VectorXd _previousSolution;

  std::vector<std::vector<FBFixedSizeAverageCalculator<double>*>> _matrixSource;
};

#endif
