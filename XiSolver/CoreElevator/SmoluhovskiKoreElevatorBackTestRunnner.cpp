#include "SmoluhovskiKoreElevatorBackTestRunnner.h"

class KBasisIntegralFunc
{
public:
  KBasisIntegralFunc(std::vector<ISmoluhovskiKoreFunc*> ipbasis, double* ipBasisCoefs)
  {
    _pbasis = ipbasis;
    _pBasisCoefs = ipBasisCoefs;
  }

  double EvalueateK(double x, double y)
  {
    double res = 0;
    for(unsigned int  i = 0; i< _pbasis.size(); i++) {
      res += _pBasisCoefs[i] * _pbasis[i]->K(x, y);
    }
    return res;
  }

  std::vector<ISmoluhovskiKoreFunc*> _pbasis; 
  double* _pBasisCoefs;
};

class KBasisDestructionOperator : public KBasisIntegralFunc, public IDestructionOperator
{
public:
  KBasisDestructionOperator(std::vector<ISmoluhovskiKoreFunc*> ipbasis, std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDiff, double* ipBasisCoefs)
    :KBasisIntegralFunc(ipbasis, ipBasisCoefs)
  {
    _basisPer = iBasisPer;
    _basisDiff= iBasisDiff;
  }

  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < n; i++) {
      res += ipArray[i] * EvalueateK(pMasterData->pRadius[i] , pMasterData->pRadius[iCurrentIDx]);
    }
    //res*= ipArray[iCurrentIDx];
    res = res * pMasterData->RadiusStep;

    return res;

    double perVal = 0;
    for(unsigned int i = 0; i < _basisPer.size(); i++) {
      perVal += _basisPer[i]->F(pMasterData->pRadius[iCurrentIDx]) * _pBasisCoefs[_pbasis.size() + i];
    }

    double dval = 0;
    for(unsigned int i = 0; i < _basisDiff.size(); i++) {
      dval += _basisDiff[i]->F(pMasterData->pRadius[iCurrentIDx]) * _pBasisCoefs[_pbasis.size() + _basisPer.size() + i];
    }


   
    if(iCurrentIDx != n - 1 && iCurrentIDx != 0) {

      res = res + perVal * ipArray[iCurrentIDx] / pMasterData->RadiusStep;


      double p = perVal * (ipArray[iCurrentIDx + 1] - ipArray[iCurrentIDx]) / pMasterData->RadiusStep;
      double d = dval * (ipArray[iCurrentIDx - 1] - 2* ipArray[iCurrentIDx] + ipArray[iCurrentIDx + 1]) / (pMasterData->RadiusStep * pMasterData->RadiusStep);

      /*
      if(p < 0) {
        res = res - p;
      }
      if(d < 0) {
        res = res - d;
      }
      */

      /*
      //res = res + perVal * (ipArray[iCurrentIDx + 1] - ipArray[iCurrentIDx]);/// pMasterData->RadiusStep;
      res = res + (ipArray[iCurrentIDx + 1]) * perVal  / (pMasterData->RadiusStep);
      res = res + 2* ipArray[iCurrentIDx] * dval  / (pMasterData->RadiusStep * pMasterData->RadiusStep);
      */
    }
   

    return res;
  }
  std::vector<IUnaryFunc*> _basisPer;
  std::vector<IUnaryFunc*> _basisDiff;
};

class KBasisProductionOperator : public KBasisIntegralFunc, public IProductionOperator
{
public:
  KBasisProductionOperator(std::vector<ISmoluhovskiKoreFunc*> ipbasis, std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDiff, double* ipBasisCoefs)
    :KBasisIntegralFunc(ipbasis, ipBasisCoefs)
  {
    _basisPer = iBasisPer;
    _basisDiff= iBasisDiff;
  }

  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < iCurrentIDx; i++) {
      res += ipArray[iCurrentIDx - i] * ipArray[i] * EvalueateK(pMasterData->pRadius[i] , pMasterData->pRadius[iCurrentIDx]);
    }
    res = res/2;
    res = res * pMasterData->RadiusStep;

    return res;

    double perVal = 0;
    for(unsigned int i = 0; i < _basisPer.size(); i++) {
      perVal += _basisPer[i]->F(pMasterData->pRadius[iCurrentIDx]) * _pBasisCoefs[_pbasis.size() + i];
    }

    double dval = 0;
    for(unsigned int i = 0; i < _basisDiff.size(); i++) {
      dval += _basisDiff[i]->F(pMasterData->pRadius[iCurrentIDx]) * _pBasisCoefs[_pbasis.size() + _basisPer.size() + i];
    }

    if(iCurrentIDx != n - 1 && iCurrentIDx != 0) {
      double p = perVal * (ipArray[iCurrentIDx + 1] - ipArray[iCurrentIDx]) / pMasterData->RadiusStep;
      double d = dval * (ipArray[iCurrentIDx - 1] - 2* ipArray[iCurrentIDx] + ipArray[iCurrentIDx + 1]) / (pMasterData->RadiusStep * pMasterData->RadiusStep);

      res = res + perVal * ipArray[iCurrentIDx + 1] / pMasterData->RadiusStep;

      /*
      if(p > 0) {
        res = res + p;
      }
      if(d > 0) {
        res = res + d;
      }
      */

      //res = res + dval * (ipArray[iCurrentIDx - 1] - 2* ipArray[iCurrentIDx] + ipArray[iCurrentIDx + 1]) / (pMasterData->RadiusStep * pMasterData->RadiusStep);
      //res = res + (ipArray[iCurrentIDx - 1] + ipArray[iCurrentIDx + 1]) * dval / (pMasterData->RadiusStep * pMasterData->RadiusStep);
      //res = res +  ipArray[iCurrentIDx] * perVal / (pMasterData->RadiusStep);
    }


    return res;
  }
   std::vector<IUnaryFunc*> _basisPer;
   std::vector<IUnaryFunc*> _basisDiff;
};




XiSolverEqualationData* CreateEqData(std::vector<ISmoluhovskiKoreFunc*> ipbasis, std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDiff, double* ipBasisCoefs) 
{
  XiSolverEqualationData* pRes = new XiSolverEqualationData();
  pRes->pDestruction = new KBasisDestructionOperator(ipbasis, iBasisPer, iBasisDiff, ipBasisCoefs);
  pRes->pProduction = new KBasisProductionOperator(ipbasis, iBasisPer, iBasisDiff, ipBasisCoefs);
  return pRes;
}

#include "ImageDataReader.h"
#include <math.h>
#include <limits>

#include "XiQSSASolver.h"

class XiOpAddSolver : public XiQSSASolver
{
public:
  XiOpAddSolver(std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDiff, double* pData)
  {
    _basisPer = iBasisPer;
    _basisDiff= iBasisDiff;
    _pData = pData;
  }

  virtual void MakeIteration(double* ipSourceValue, double* ipTargetValue)
  {
    XiQSSASolver::MakeIteration(ipSourceValue, ipTargetValue);

    unsigned int n = _pInput->RadiusCount;

    for(unsigned int k = 1; k < n - 1; k++) {
      double perVal = 0;
      for(unsigned int i = 0; i < _basisPer.size(); i++) {
        perVal += _basisPer[i]->F(_pInput->pRadius[k]) * _pData[i];
      }

      double dval = 0;
      for(unsigned int i = 0; i < _basisDiff.size(); i++) {
        dval += _basisDiff[i]->F(_pInput->pRadius[k]) * _pData[_basisPer.size() + i];
      }

    //  perVal = - _pInput->RadiusStep * 0.001;
     // dval =  0.008;

      //dval = 0.001;
      perVal = 0.0017;

      double p = perVal * (ipSourceValue[k - 1] - ipSourceValue[k]);// / _pInput->RadiusStep;
      double d = dval * (ipSourceValue[k - 1] - 2* ipSourceValue[k] + ipSourceValue[k + 1]);/// (_pInput->RadiusStep * _pInput->RadiusStep);


      ipTargetValue[k] = (ipTargetValue[k]  +  ipSourceValue[k] + (p + d) * _pInput->TimeStep) / 2;
      if(ipTargetValue[k] < 0) ipTargetValue[k] = 0;

    }
  }

  double* _pData;
  std::vector<IUnaryFunc*> _basisPer;
  std::vector<IUnaryFunc*> _basisDiff;
};


void RunCoreElevationBackTest(NValue* ipData, IDRRGBColorGradientInterpretator* ipColor, std::vector<ISmoluhovskiKoreFunc*> ipbasis, std::vector<IUnaryFunc*> iBasisPer, std::vector<IUnaryFunc*> iBasisDiff, IXiSolver* ipSolver, const char* ipOutFile)
{
  SmoluhovskiKoreElevator kv;
  double* pres = kv.Evaluate(ipbasis,iBasisPer, iBasisDiff, *ipData);


  ipSolver = new XiOpAddSolver(iBasisPer, iBasisDiff, &pres[ipbasis.size()]);

  double s = 0;
  for(unsigned int i = 0; i < ipbasis.size() + iBasisPer.size() + iBasisDiff.size(); i++) {
    printf(" %f ", pres[i]);
    s += pres[i] > 0 ? pres[i] : -pres[i];
  }
  printf(" %s \n ", ipOutFile);

 // if(s != s || s > 1.00E006) {
//    return;
 // }

 // pres[0] = 1.00;
  // pres[1] = 1;

  unsigned int nRad = ipData->xCount;
  XiSolverInput input;

  input.RadiusCount = nRad;
  input.TimeStep = ipData->tStep;
  //input.TimeStep = input.
  input.TimeStepCount = ipData->tCount;

  input.pRadius = new double[nRad];
  input.pInitialDistribution = new double[nRad];// ipData->pVals[0];

  memcpy(input.pRadius, ipData->pRads, sizeof(double) * nRad);
  memcpy(input.pInitialDistribution, ipData->pVals[0], sizeof(double) * nRad);

  input.pEqualationData = CreateEqData(ipbasis,iBasisPer, iBasisDiff, pres);

  input.pEqualationData->pProduction->pMasterData = &input;
  input.pEqualationData->pDestruction->pMasterData = &input;
  input.RadiusStep = ipData->xStep;

  
  XiSolverOutput* pOut = ipSolver->Solve(&input);



  printf("SAVING\n");
//  RGBColorGradientInterpretator gi(iSettings.pSourceGradFile, iSettings.gradMinVal, iSettings.gradMaxVal);

  CImg<unsigned char> res(pOut->pInput->TimeStepCount, pOut->pInput->RadiusCount, 1, 3);

  for(unsigned int t =  0; t < pOut->pInput->TimeStepCount; t++) {
    for(unsigned int r = 0; r < pOut->pInput->RadiusCount; r++) {
      IDRSetPixel(res, t, r, ipColor->ToColor(pOut->pFinalDistribution[t][pOut->pInput->RadiusCount - r - 1]));
    }
  }

  //res.normalize(0, 255);
  //CImgDisplay disp;
  //disp.display(res, );

  res.save(ipOutFile);

}