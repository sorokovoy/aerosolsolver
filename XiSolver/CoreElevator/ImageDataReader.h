#ifndef ImageDataReader_H
#define ImageDataReader_H

#include "CImg.h"
using namespace cimg_library;

class IXiSolver;
struct XiSolverEqualationData;

struct IDRRGBColor
{
  unsigned char r;
  unsigned char g;
  unsigned char b;
};

int IDRGetDistance(const IDRRGBColor& iV1, const IDRRGBColor& iV2);
IDRRGBColor IDRGetPixel(const CImg<unsigned char>& iFrom, int x, int y);
void IDRSetPixel(CImg<unsigned char>& iFrom, int x, int y, const IDRRGBColor& v);

class IDRRGBColorGradientInterpretator
{
public:
  IDRRGBColorGradientInterpretator(const char* ipGradFile, double iMinValue = 0, double iMaxValue = 1);
  double Recognize(const IDRRGBColor& iV);
  IDRRGBColor ToColor(double v);

private:
  CImg<unsigned char>* _pImage;
  double _minV;
  double _maxV;
};

struct IDRImageDataReaderReasult
{
	double** pData;
	unsigned int dataRows;
	unsigned int dataColums;
};

class IDRImageDataReader
{
public:
	IDRImageDataReaderReasult ReadFile(const char* ipFile, IDRRGBColorGradientInterpretator* ipGradInterpriter);
};

#endif