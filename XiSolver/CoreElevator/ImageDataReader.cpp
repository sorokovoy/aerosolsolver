#include "ImageDataReader.h"

#define ABS2FF(x) ((x) > 0 ? (x) : -(x))

int IDRGetDistance(const IDRRGBColor& iV1, const IDRRGBColor& iV2)
{
  return (int)ABS2FF((int)iV1.b - (int)iV2.b) + (int)ABS2FF((int)iV1.g - (int)iV2.g) + (int)ABS2FF((int)iV1.r - (int)iV2.r);
}

IDRRGBColor IDRGetPixel(const CImg<unsigned char>& iFrom, int x, int y)
{
  IDRRGBColor v;
  v.r = *iFrom.data(x, y, 0, 0);
  v.g = *iFrom.data(x, y, 0, 1);
  v.b = *iFrom.data(x, y, 0, 2);
  return v;
}

void IDRSetPixel(CImg<unsigned char>& iFrom, int x, int y, const IDRRGBColor& v)
{
  *iFrom.data(x, y, 0, 0) = v.r;
  *iFrom.data(x, y, 0, 1) = v.g;
  *iFrom.data(x, y, 0, 2) = v.b;
}


IDRRGBColorGradientInterpretator::IDRRGBColorGradientInterpretator(const char* ipGradFile, double iMinValue, double iMaxValue)
{
  _pImage = new CImg<unsigned char>(ipGradFile);
  _minV = iMinValue;
  _maxV = iMaxValue;
}

double IDRRGBColorGradientInterpretator::Recognize(const IDRRGBColor& iV)
{
  int mid = 255000;
  int idx = 0;
  for(int i = 0; i < _pImage->width(); i++) {
    int v = IDRGetDistance(IDRGetPixel(*_pImage, i, 0), iV);
    if(mid > v) {
      mid = v;
      idx = i;
    }
  }
  return  ((double) idx) / (_pImage->width()) * (_maxV - _minV) + _minV;
}

IDRRGBColor IDRRGBColorGradientInterpretator::ToColor(double v)
{
  v = (v - _minV) / (_maxV - _minV);
  if(v >= 1) {
    v = 0.9999;
  }
  if(v < 0) {
    v = 0;
  }
  v = v * _pImage->width();
  return IDRGetPixel(*_pImage, (int)v, 0);
}

IDRImageDataReaderReasult IDRImageDataReader::ReadFile(const char* ipFile, IDRRGBColorGradientInterpretator* ipGradInterpriter)
{
  IDRImageDataReaderReasult res;
  CImg<unsigned char> src(ipFile);
  res.dataRows = src.height();
  res.dataColums = src.width();
  res.pData = new double* [res.dataColums];

  for(unsigned int i = 0; i < res.dataColums; i++) {
    res.pData[i] = new double[res.dataRows];
    for(unsigned int j = 0; j < res.dataRows; j++) {
      //unsigned char* ptr = src.data(i, j);
      res.pData[i][j] = ipGradInterpriter->Recognize(IDRGetPixel(src, i, res.dataRows - j - 1));
    }
  }
  return res;
}