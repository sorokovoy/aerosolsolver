#include "XiQSSA2OrderSolver.h"
#include <math.h>
#include <stdio.h>

void XiQSSA2OrderSolver::MakeIteration(double* ipSourceValue, double* ipTargetValue)
{
  unsigned int n = _pInput->RadiusCount;
  double* pDestruction0 = new double[n];
  double* pProduction0 = new double[n];
  EvaluateProduction(ipSourceValue, pProduction0);
  EvaluateDestruction(ipSourceValue, pDestruction0);

  double* ipTargetValueClone = new double[n];
  XiQSSASolver::MakeIteration(ipSourceValue, ipTargetValue);
  for(unsigned int i = 0; i < n; i++) 
    ipTargetValueClone[i] = ipTargetValue[i];

  double* pDestructionT = new double[n];
  double* pProductionT = new double[n];
  EvaluateProduction(ipTargetValue, pProductionT);
  EvaluateDestruction(ipTargetValue, pDestructionT);

  for(unsigned int i = 0; i < n; i++) {
    double psiIntegral0 = (pDestruction0[i] + pDestructionT[i]) * _pInput->TimeStep / 2;
    psiIntegral0 = exp(-psiIntegral0);
    double integralVal = (pProduction0[i]*psiIntegral0 + pProductionT[i])* _pInput->TimeStep/2;
    ipTargetValue[i] = psiIntegral0*ipSourceValue[i] + integralVal;
  }

  double diff = 0;
  for(unsigned int i = 0; i < n; i++) 
    diff += (ipTargetValueClone[i] - ipTargetValue[i]) * (ipTargetValueClone[i] - ipTargetValue[i]);
 // printf(" %E \n ", diff);


  delete pDestruction0;
  delete pProduction0;
  delete pDestructionT;
  delete pProductionT;
}


