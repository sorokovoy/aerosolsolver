#ifndef XiSimple2OrderSolver_H
#define XiSimple2OrderSolver_H

#include "XiSimpleSolver.h"

class XiSimple2OrderSolver : public XiSimpleSolver
{
protected:
  virtual void MakeIteration(double* ipSourceValue, double* ipTargetValue);
};

#endif