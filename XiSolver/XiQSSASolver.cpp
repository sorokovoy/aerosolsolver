#include "XiQSSASolver.h"
#include <math.h>

void XiQSSASolver::MakeIteration(double* ipSourceValue, double* ipTargetValue)
{
  unsigned int n = _pInput->RadiusCount;
  double* pDestruction = new double[n];
  double* pProduction = new double[n];

  EvaluateProduction(ipSourceValue, pProduction);
  EvaluateDestruction(ipSourceValue, pDestruction);

  printArray("PROD", pDestruction);
  printArray("DEST", pProduction);

  for(unsigned int i = 0; i < n; i++) {
    double arg = pDestruction[i] * _pInput->TimeStep;
    double expV = exp(-arg);
    double v1 = ipSourceValue[i] * expV;
    double v2 = 0;
    if(abs(pDestruction[i]) > 1.00E-010) {
      v2 = (1.00 - expV) * pProduction[i] / (pDestruction[i]);
    } else {
      v2 = pProduction[i] * expV * _pInput->TimeStep;
    }
    ipTargetValue[i] = v1 + v2/2;
  }
  delete pDestruction;
  delete pProduction;
}


