#include "XiSimpleSolver.h"
#include <math.h>

void XiSimpleSolver::MakeIteration(double* ipSourceValue, double* ipTargetValue)
{
  unsigned int n = _pInput->RadiusCount;
  double* pDestruction = new double[n];
  double* pProduction = new double[n];

  EvaluateProduction(ipSourceValue, pProduction);
  EvaluateDestruction(ipSourceValue, pDestruction);

  printArray("PROD", pDestruction);
  printArray("DEST", pProduction);

 // _pInput->TimeStep = 1;

  for(unsigned int i = 0; i < n; i++) {
    double arg = pDestruction[i] * _pInput->TimeStep;
    double psiVal = exp(-arg);
    double v1 = ipSourceValue[i] * psiVal;
    double v2 = pProduction[i] * psiVal * _pInput->TimeStep;
    ipTargetValue[i] = v1 + v2;
  }
  delete pDestruction;
  delete pProduction;
}


