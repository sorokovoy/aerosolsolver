
#include "stdio.h"
#include <string.h>
#include <math.h>
#include <typeinfo.h>

#include "IXiSolver.h"

XiSolverInput::~XiSolverInput()
{
  delete pRadius;
  delete pInitialDistribution;
  delete pEqualationData;
}

XiSolverOutput::~XiSolverOutput()
{
  unsigned int n = pInput->TimeStepCount;
  for(unsigned int i = 0; i < n; i++) {
    delete pFinalDistribution[i];
  }
  delete pFinalDistribution;
  delete times;
}

XiSolverOutput* IXiSolver::Solve(XiSolverInput* iInput)
{
  printf("%s\n", typeid(*this).name());
  _pInput = iInput;
  XiSolverOutput* pRes = CreateContainer(iInput);
  double _time = 0;
  for(unsigned int t = 0; t < iInput->TimeStepCount - 1; t++) {
    MakeIteration(pRes->pFinalDistribution[t], pRes->pFinalDistribution[t + 1]);
    _time += _pInput->TimeStep;
    pRes->times[t+1] = _time;
  }
  return pRes;
}

XiSolverOutput* IXiSolver::CreateContainer(XiSolverInput* iInput)
{
  XiSolverOutput* pResult = new XiSolverOutput();
  pResult->pInput = iInput;

  pResult->pFinalDistribution =  new double*[pResult->pInput->TimeStepCount];
  pResult->times = new double[pResult->pInput->TimeStepCount];
  for(unsigned int t = 0; t < pResult->pInput->TimeStepCount; t++) {
    pResult->times[t] = 0;
    pResult->pFinalDistribution[t] = new double[pResult->pInput->RadiusCount];
    for(unsigned int r = 0; r < pResult->pInput->RadiusCount; r++) {
       pResult->pFinalDistribution[t][r] = 0;
    }
  }

  memcpy(pResult->pFinalDistribution[0], iInput->pInitialDistribution, sizeof(double) * iInput->RadiusCount);
  return pResult;
}

void IXiSolver::printArray(char* preffix, double* vals)
{
  return;
  printf("A-%7s ", preffix);
  unsigned int N = _pInput->RadiusCount;
  N = 10;
  for(unsigned int r = 0; r < N; r++) {
    printf("%E ", vals[r]);
  }
  printf("\n");
}

void IXiSolver::EvaluateDestruction(double* ipSourceValue, double* iopDestruction)
{
  int n = (int)_pInput->RadiusCount;
 // #pragma omp parallel for
  for(int i = 0; i < n; i++) {
    iopDestruction[i] = _pInput->pEqualationData->pDestruction->Evaluate(ipSourceValue, i);
  }
}

void IXiSolver::EvaluateProduction(double* ipSourceValue, double* iopProduction)
{
  int n = (int)_pInput->RadiusCount;
//  #pragma omp parallel for
  for(int i = 0; i < n; i++) {
    iopProduction[i] = _pInput->pEqualationData->pProduction->Evaluate(ipSourceValue, i);
  }
}
