#include "XIDensyImageGenerator.h"

#include "IXiSolver.h"



#define ABS2FF(x) ((x) > 0 ? (x) : -(x))

int GetDistance(const RGBColor& iV1, const RGBColor& iV2)
{
  return (int)ABS2FF((int)iV1.b - (int)iV2.b) + (int)ABS2FF((int)iV1.g - (int)iV2.g) + (int)ABS2FF((int)iV1.r - (int)iV2.r);
}

RGBColor Get(const CImg<unsigned char>& iFrom, int x, int y)
{
  RGBColor v;
  v.r = *iFrom.data(x, y, 0, 0);
  v.g = *iFrom.data(x, y, 0, 1);
  v.b = *iFrom.data(x, y, 0, 2);
  return v;
}

void Set(CImg<unsigned char>& iFrom, int x, int y, const RGBColor& v)
{
  *iFrom.data(x, y, 0, 0) = v.r;
  *iFrom.data(x, y, 0, 1) = v.g;
  *iFrom.data(x, y, 0, 2) = v.b;
}


RGBColorGradientInterpretator::RGBColorGradientInterpretator(const char* ipGradFile, double iMinValue, double iMaxValue)
{
  _pImage = new CImg<unsigned char>(ipGradFile);
  _minV = iMinValue;
  _maxV = iMaxValue;
}

double RGBColorGradientInterpretator::Recognize(const RGBColor& iV)
{
  int mid = 255000;
  int idx = 0;
  for(int i = 0; i < _pImage->width(); i++) {
    int v = GetDistance(Get(*_pImage, i, 0), iV);
    if(mid > v) {
      mid = v;
      idx = i;
    }
  }
  return  ((double) idx) / (_pImage->width()) * (_maxV - _minV) + _minV;
}

RGBColor RGBColorGradientInterpretator::ToColor(double v)
{
  v = (v - _minV) / (_maxV - _minV);
  if(v >= 1) {
    v = 0.9999;
  }
  if(v < 0) {
    v = 0;
  }
  v = v * _pImage->width();
  return Get(*_pImage, (int)v, 0);
}

void XIDensyImageGenerator::Generate(XiSolverEqualationData* ipEqData, IXiSolver*  ipSolver, const XIDensyImageGeneratorSettings& iSettings)
{
  unsigned int nRad = iSettings.nRadCount;
  XiSolverInput input;

  input.RadiusCount = nRad;
  input.TimeStep = iSettings.timeStep;// 0.00001;
  input.TimeStepCount = iSettings.timeStepCount;// 2000;
  input.pRadius = new double[nRad];
  input.pInitialDistribution = new double[nRad];
  input.pEqualationData = ipEqData;

  input.pEqualationData->pProduction->pMasterData = &input;
  input.pEqualationData->pDestruction->pMasterData = &input;

  double lineVal = iSettings.lineValue;// 1.00E007;
  double zeroVal = iSettings.lineZeroVal;// 1.00E004;

  double rad0 = iSettings.rad0;// 5.00E-010;
  input.RadiusStep = iSettings.radStep; // 5.00E-006;
  for(unsigned int i = 0; i < nRad; i++) {
    input.pRadius[i] = rad0 + input.RadiusStep * i;
    if(nRad / 10  < i && i < 2 * nRad / 10) {
      double expv = exp( -(((double)i - 3.00 * nRad / 20.00) * ((double)i - 3.00 * nRad / 20.00) ) / ((nRad / 60) * (nRad / 60)));
      input.pInitialDistribution[i] = expv * (lineVal - zeroVal) + zeroVal;

    } else {
      input.pInitialDistribution[i] = zeroVal;
    }
  }

//  IXiSolver* ipSolver = new XiSimpleSolver();

  XiSolverOutput* pOut = ipSolver->Solve(&input);

  printf("SAVING\n");
  RGBColorGradientInterpretator gi(iSettings.pSourceGradFile, iSettings.gradMinVal, iSettings.gradMaxVal);

  CImg<unsigned char> res(pOut->pInput->TimeStepCount, pOut->pInput->RadiusCount, 1, 3);

  for(unsigned int t =  0; t < pOut->pInput->TimeStepCount; t++) {
    for(unsigned int r = 0; r < pOut->pInput->RadiusCount; r++) {
      Set(res, t, r, gi.ToColor(pOut->pFinalDistribution[t][pOut->pInput->RadiusCount - r - 1]));
    }
  }

  //res.normalize(0, 255);
  //CImgDisplay disp;
  //disp.display(res, );

  res.save(iSettings.pOutFile);
}