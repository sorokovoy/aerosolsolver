#ifndef IXiSolver_H
#define IXiSolver_H

struct XiSolverInput;

class IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx) = 0;
  XiSolverInput* pMasterData;
};

class IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx) = 0;
  XiSolverInput* pMasterData;
};

struct XiSolverEqualationData
{
  IDestructionOperator* pDestruction;
  IProductionOperator* pProduction;
};

struct XiSolverInput
{
  ~XiSolverInput();
  unsigned int RadiusCount;
  unsigned int TimeStepCount;
  double TimeStep;
  double RadiusStep;
  double* pRadius;
  double* pInitialDistribution;
  XiSolverEqualationData* pEqualationData;
};

struct XiSolverOutput
{
  ~XiSolverOutput();
  XiSolverInput* pInput;
  double** pFinalDistribution;
  double* times;
};

class IXiSolver
{
public:
  XiSolverOutput* Solve(XiSolverInput* iInput);

protected:
  virtual void MakeIteration(double* ipSourceValue, double* ipTargetValue) = 0;

  virtual void EvaluateDestruction(double* ipSourceValue, double* iopDestruction);
  virtual void EvaluateProduction(double* ipSourceValue, double* iopProduction);

  XiSolverOutput* CreateContainer(XiSolverInput* iInput);
  void printArray(char* preffix, double* vals);

  XiSolverInput* _pInput;
};

#endif
