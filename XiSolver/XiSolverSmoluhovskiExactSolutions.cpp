
#include "XiSolverSmoluhovskiExactSolutions.h"
#include <stdio.h>
#include <math.h>

#define AS_Pi 3.14159265358979323846
#define ASMax(a,b)    (((a) > (b)) ? (a) : (b))
#define ASMin(a,b)    (((a) < (b)) ? (a) : (b))

double SmoluhovskiCore1::Solution(double x, double t)
{
  double NT = 2.0 / (2.00  + 1.00*t);
  return NT*NT * exp(-NT *x);
}

double SmoluhovskiCore1::Boundary(double x)
{
  return exp(-x);
}

class SmoluhovskiCore1IDestructionOperator : public IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < n - 1; i++) {
      res += (ipArray[i] + ipArray[i + 1]) / 2;
    }
    res = res * pMasterData->RadiusStep;
    return res;
  }
};

class SmoluhovskiCore1IProductionOperator : public IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(int i = 0; i < (int)iCurrentIDx - 1; i++) {
      res += (ipArray[iCurrentIDx - i] * ipArray[i] + ipArray[iCurrentIDx - i - 1] * ipArray[i + 1]) / 2;
    }
    res = res/2;
    res = res * pMasterData->RadiusStep;
    return res;
  }
};


XiSolverEqualationData* SmoluhovskiCore1::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new SmoluhovskiCore1IProductionOperator();
  pData->pDestruction = new SmoluhovskiCore1IDestructionOperator();
  return pData;
}

///================================================================================================================================================

double SmoluhovskiCoreXY::Solution(double x, double t)
{
  if(t < 0.0001) {
    return Boundary(x);
  }

  double T;
  if(t <= 1) {
    T = 1 + t;
  } else {
    T = 2*sqrt(t);
  }

  double integralXArg = 2 * x * sqrt(t);
  double IX = _j1( integralXArg);  // _j1 bessel function furst kind

  double v1 = exp(-T *x ) * IX;
  v1 = v1 / (x * x * sqrt(t));
  return v1;
}

double SmoluhovskiCoreXY::Boundary(double x)
{
  return exp(-x)/x;
}

class SmoluhovskiCoreXYIDestructionOperator : public IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < n; i++) {
      res += ipArray[i] * pMasterData->pRadius[i] * pMasterData->pRadius[iCurrentIDx];
    }
    //res*= ipArray[iCurrentIDx];
    res = res * pMasterData->RadiusStep;
    return res;
  }
};

class SmoluhovskiCoreXYIProductionOperator : public IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < iCurrentIDx; i++) {
      res += ipArray[iCurrentIDx - i] * ipArray[i] * pMasterData->pRadius[iCurrentIDx - i] * pMasterData->pRadius[i];
    }
    res = res/2;
    res = res * pMasterData->RadiusStep;
    return res;
  }
};


XiSolverEqualationData* SmoluhovskiCoreXY::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new SmoluhovskiCoreXYIProductionOperator();
  pData->pDestruction = new SmoluhovskiCoreXYIDestructionOperator();
  return pData;
}

///================================================================================================================================================

double SmoluhovskiCoreAdditive::Solution(double x, double t)
{
  double v = pow(x, -1.50) * exp(-t) * exp( - exp(-2*t) * x / 2);
  v = v/(sqrt( 2 * AS_Pi));
  return v;
}

double SmoluhovskiCoreAdditive::Boundary(double x)
{
  return Solution(x, 0);
}

class SmoluhovskiCoreAdditiveIDestructionOperator : public IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < n; i++) {
      res += ipArray[i] * (pMasterData->pRadius[i] + pMasterData->pRadius[iCurrentIDx]);
    }
    //res*= ipArray[iCurrentIDx];
    res = res * pMasterData->RadiusStep;
    return res;
  }
};

class SmoluhovskiCoreAdditiveIProductionOperator : public IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < iCurrentIDx; i++) {
      res += ipArray[iCurrentIDx - i] * ipArray[i] * (pMasterData->pRadius[iCurrentIDx - i] + pMasterData->pRadius[i]);
    }
    res = res/2;
    res = res * pMasterData->RadiusStep;
    return res;
  }
};


XiSolverEqualationData* SmoluhovskiCoreAdditive::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new SmoluhovskiCoreAdditiveIProductionOperator();
  pData->pDestruction = new SmoluhovskiCoreAdditiveIDestructionOperator();
  return pData;
}
