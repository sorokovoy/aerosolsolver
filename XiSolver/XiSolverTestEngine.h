#ifndef XiSolverTestEngine_H
#define XiSolverTestEngine_H

#include "IXiSolver.h"

class IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t) = 0;
  virtual double Boundary(double x) = 0;
  virtual XiSolverEqualationData* CreateEqualationData() = 0;
};

class XiSolverTestEngine
{
public:
  void RunTest(IXiSolver* ipSolver, IXiSolverTestEnginerExactSolution* iTarget);
};


#endif