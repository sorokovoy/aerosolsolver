
#include "XiSolverExactSolutions.h"
#include <stdio.h>
#include <math.h>

#define AS_Pi 3.14159265358979323846
#define ASMax(a,b)    (((a) > (b)) ? (a) : (b))
#define ASMin(a,b)    (((a) < (b)) ? (a) : (b))

double Solution1::Solution(double x, double t)
{
  return exp(-t)*x;
}

double Solution1::Boundary(double x)
{
  return Solution(x, 0);
}

class Solution1IDestructionOperator : public IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return 1;
  }
};

class Solution1IProductionOperator : public IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return 0;
  }
};


XiSolverEqualationData* Solution1::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new Solution1IProductionOperator();
  pData->pDestruction = new Solution1IDestructionOperator();
  return pData;
}

///================================================================================================================================================

double Solution2::Solution(double x, double t)
{
  double v1 = x;
  double v2 = (1 - x) * exp(-t) + x;
  return v1/v2;
}

double Solution2::Boundary(double x)
{
  return Solution(x, 0);
}

class Solution2IDestructionOperator : public IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return ipArray[iCurrentIDx];
  }
};

class Solution2IProductionOperator : public IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return ipArray[iCurrentIDx];
  }
};


XiSolverEqualationData* Solution2::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new Solution2IProductionOperator();
  pData->pDestruction = new Solution2IDestructionOperator();
  return pData;
}

///================================================================================================================================================

double Solution3::Solution(double x, double t)
{
  return x / (1 + t*x);
}

double Solution3::Boundary(double x)
{
  return Solution(x, 0);
}

class Solution3IDestructionOperator : public IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return ipArray[iCurrentIDx];
  }
};

class Solution3IProductionOperator : public IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return 0;
  }
};


XiSolverEqualationData* Solution3::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new Solution3IProductionOperator();
  pData->pDestruction = new Solution3IDestructionOperator();
  return pData;
}

///================================================================================================================================================

double Solution4::Solution(double x, double t)
{
  double v1 = (x - 1)*exp(-2*t) + 1 + x;
  double v2 = -(x - 1)*exp(-2*t) + 1 + x;
  return  v1/v2;
}

double Solution4::Boundary(double x)
{
  return Solution(x, 0);
}

class Solution4IDestructionOperator : public IDestructionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return ipArray[iCurrentIDx];
  }
};

class Solution4IProductionOperator : public IProductionOperator
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    return 1;
  }
};


XiSolverEqualationData* Solution4::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new Solution4IProductionOperator();
  pData->pDestruction = new Solution4IDestructionOperator();
  return pData;
}