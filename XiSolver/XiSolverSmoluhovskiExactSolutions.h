#ifndef XiSolverSmoluhovskiExactSolutions_H
#define XiSolverSmoluhovskiExactSolutions_H

#include "XiSolverTestEngine.h"

class SmoluhovskiCore1 : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};


class SmoluhovskiCoreXY : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};

class SmoluhovskiCoreAdditive : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};


#endif 