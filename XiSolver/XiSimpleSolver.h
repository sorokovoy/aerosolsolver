#ifndef XiSimpleSolver_H
#define XiSimpleSolver_H

#include "IXiSolver.h"

class XiSimpleSolver : public IXiSolver
{
protected:
  virtual void MakeIteration(double* ipSourceValue, double* ipTargetValue);
};

#endif