#ifndef XiSolverExactSolutions_H
#define XiSolverExactSolutions_H

#include "XiSolverTestEngine.h"

class Solution1 : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};


class Solution2 : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};

class Solution3 : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};

class Solution4 : public IXiSolverTestEnginerExactSolution
{
public:
  virtual double Solution(double x, double t);
  virtual double Boundary(double x);
  virtual XiSolverEqualationData* CreateEqualationData();
};




#endif 