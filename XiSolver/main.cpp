
#include "XiQSSASolver.h"
#include "XiQSSA2OrderSolver.h"
#include "XiSimpleSolver.h"
#include "XiSimple2OrderSolver.h"
#include "XiSolverTestEngine.h"
#include "XiSolverExactSolutions.h"
#include "XiSolverSmoluhovskiExactSolutions.h"

#include "GeneralCoreEquation.h"

#include "XIDensyImageGenerator.h"

void RunKoreBackTest();


void main(void)
{
  XiSolverTestEngine t;
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCore1());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCore1());
  
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreXY());
  return;


  t.RunTest(new XiSimpleSolver(), new SmoluhovskiCore1());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCore1());

  t.RunTest(new XiSimpleSolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreXY());

  t.RunTest(new XiSimpleSolver(), new SmoluhovskiCoreAdditive());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreAdditive());

  printf("------------------------------------------------------------------------------------------------------------- \n");

  t.RunTest(new XiSimple2OrderSolver(), new SmoluhovskiCore1());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCore1());

  t.RunTest(new XiSimple2OrderSolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreXY());

  t.RunTest(new XiSimple2OrderSolver(), new SmoluhovskiCoreAdditive());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreAdditive());
  return;

  RunKoreBackTest();
  return;

  XIDensyImageGeneratorSettings s;
  s.gradMaxVal = 1.00E006;
  s.gradMinVal = 0;
  s.rad0 = 5.00E-010;
  s.radStep = 5.00E-006;

  s.lineValue   = 1.00E007;
  s.lineZeroVal = 1.00E004;

  s.nRadCount = 2000;
  s.timeStep = 0.0001;
  s.timeStepCount = 2000;

  s.pSourceGradFile = "grad.bmp";
  XIDensyImageGenerator dg;
  IXiSolverTestEnginerExactSolution* pS;

  if(0) {

  IXiSolverTestEnginerExactSolution* pS = new SmoluhovskiCore1();
  s.pOutFile = "K1_XiSimpleSolver.bmp";
  s.timeStep = 0.0000005;
  dg.Generate(pS->CreateEqualationData(),new XiSimpleSolver(), s);

  pS = new SmoluhovskiCoreXY();
  s.pOutFile = "KXY_XiSimpleSolver.bmp";
  s.timeStep = 0.1;
  dg.Generate(pS->CreateEqualationData(),new XiSimpleSolver(), s);

  pS = new SmoluhovskiCoreAdditive();
  s.timeStep = 0.0001;
  s.pOutFile = "KX_PL_Y_XiSimpleSolver.bmp";
  dg.Generate(pS->CreateEqualationData(),new XiSimpleSolver(), s);

  pS = new SmoluhovskiCore1();
  s.timeStep = 0.0000005;
  s.pOutFile = "K1_XiQSSASolver.bmp";
  dg.Generate(pS->CreateEqualationData(),new XiQSSASolver(), s);

  pS = new SmoluhovskiCoreXY();
  s.timeStep = 0.1;
  s.pOutFile = "KXY_XiQSSASolver.bmp";
  dg.Generate(pS->CreateEqualationData(),new XiQSSASolver(), s);

  pS = new SmoluhovskiCoreAdditive();
  s.timeStep = 0.0001;
  s.pOutFile = "KX_PL_Y_XiQSSAolver.bmp";
  dg.Generate(pS->CreateEqualationData(),new XiQSSASolver(), s);
  }
  
  s.rad0 = 5.00E-010;
  s.radStep = 1.00E-010;

  s.timeStepCount = 2000;
  s.gradMaxVal = 2.00E006;
  s.nRadCount = 4000;
  pS = new SmoluhovskiGeneralCore();
  s.pOutFile = "KGENERAL_XiSimpleSolver.bmp";
  s.timeStep = 1.00E009;
  dg.Generate(pS->CreateEqualationData(),new XiSimpleSolver(), s);

  pS = new SmoluhovskiGeneralCore();
  s.pOutFile = "KGENERAL_XiQSSASolver.bmp";
  dg.Generate(pS->CreateEqualationData(),new XiQSSASolver(), s);

  return;

  t.RunTest(new XiSimpleSolver(), new SmoluhovskiCore1());
  t.RunTest(new XiSimple2OrderSolver(), new SmoluhovskiCore1());
  t.RunTest(new XiSimpleSolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiSimple2OrderSolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiSimpleSolver(), new SmoluhovskiCoreAdditive());
  t.RunTest(new XiSimple2OrderSolver(), new SmoluhovskiCoreAdditive());


  t.RunTest(new XiQSSASolver(), new SmoluhovskiCore1());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCore1());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreAdditive());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreAdditive());

  return;

  t.RunTest(new XiSimpleSolver(), new Solution2());
  t.RunTest(new XiSimple2OrderSolver(), new Solution2());
  t.RunTest(new XiQSSASolver(), new Solution2());
  t.RunTest(new XiQSSA2OrderSolver(), new Solution2());

  t.RunTest(new XiSimpleSolver(), new Solution3());
  t.RunTest(new XiSimple2OrderSolver(), new Solution3());
  t.RunTest(new XiQSSASolver(), new Solution3());
  t.RunTest(new XiQSSA2OrderSolver(), new Solution3());


  t.RunTest(new XiSimpleSolver(), new Solution4());
  t.RunTest(new XiSimple2OrderSolver(), new Solution4());
  t.RunTest(new XiQSSASolver(), new Solution4());
  t.RunTest(new XiQSSA2OrderSolver(), new Solution4());

  return;


  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreAdditive());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreAdditive());



  t.RunTest(new XiSimpleSolver(), new Solution1());
  t.RunTest(new XiSimple2OrderSolver(), new Solution1());
  t.RunTest(new XiSimpleSolver(), new Solution2());
  t.RunTest(new XiSimple2OrderSolver(), new Solution2());
  t.RunTest(new XiSimpleSolver(), new Solution3());
  t.RunTest(new XiSimple2OrderSolver(), new Solution3());
  t.RunTest(new XiSimpleSolver(), new Solution4());
  t.RunTest(new XiSimple2OrderSolver(), new Solution4());
  return;



<<<<<<< HEAD
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCore1());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreXY());
  t.RunTest(new XiQSSASolver(), new SmoluhovskiCoreAdditive());
  t.RunTest(new XiQSSA2OrderSolver(), new SmoluhovskiCoreAdditive());
  return;
=======
>>>>>>> bbcddee68c240e620262ad9509e6b03a3b29b4a8
  t.RunTest(new XiQSSA2OrderSolver(), new Solution1());
  t.RunTest(new XiQSSASolver(), new Solution1());
  t.RunTest(new XiQSSASolver(), new Solution2());
  t.RunTest(new XiQSSA2OrderSolver(), new Solution2());
  t.RunTest(new XiQSSASolver(), new Solution3());
  t.RunTest(new XiQSSA2OrderSolver(), new Solution3());
  t.RunTest(new XiQSSASolver(), new Solution4());
  t.RunTest(new XiQSSA2OrderSolver(), new Solution4());
}

#include "ImageDataReader.h"
#include "SmoluhovskiKoreElevatorBackTestRunnner.h"

class KoreX : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return x;
  }
};

class KoreY : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return y;
  }
};

class KoreXY : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return y*x;
  }
};

class KoreX2 : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return x*x;
  }
};

class KoreY2 : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return y*y;
  }
};

class KoreX2Y2 : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return y*x*y*x;
  }
};

class KoreXY2 : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return y*y*x;
  }
};

class KoreX2Y : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return y*x*x;
  }
};

class KoreExpX_Y : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return exp(x + y);
  }
};

class KoreExp_m_X2_Y2 : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return exp(-x*x + y*y);
  }
};

class KoreSQRTX : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return sqrt(x);
  }
};

class KoreSQRTY : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return sqrt(y);
  }
};

class KoreSQRTX_Y : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return sqrt(x + y);
  }
};

#include "GeneralCoreEquation.h"

class KoreKG : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return Kg(x, y);
  }
};

class KoreKB : public ISmoluhovskiKoreFunc
{
public:
  virtual double K(double x, double y) const
  {
    return Kb(x, y);
//    return sqrt(x + y);
  }
};

class Fx : public IUnaryFunc
{
public:
  Fx(int deg)
  {
    _deg = deg;
  }
  virtual double F(double x) const
  {
    return pow(x, _deg);
  }

  int _deg;
};

class C : public IUnaryFunc
{
public:
  virtual double F(double x) const
  {
    return 1;
  }
};


#include "XiQSSASolver.h"


void RunKoreBackTest()
{
  IDRRGBColorGradientInterpretator gi("grad.bmp", 1.00E002, 1.00E004);
  IDRImageDataReader r;
  IDRImageDataReaderReasult res = r.ReadFile("data.bmp", &gi);
  printf("DATA readed \n");


  NValue nv;
  nv.xCount = res.dataRows;
  nv.tCount = res.dataColums - 220;

  nv.tStep = 93600/nv.tCount;
  nv.xStep = (1.00E003 / nv.xCount) * 1.00E-009;
  //nv.tStep = nv.xStep / 10;

  nv.pVals = &(res.pData[220]);


  nv.pRads = new double[nv.xCount];
  for(int x = 0; x < nv.xCount; x++) {
    nv.pRads[x] = nv.xStep * (x + 1);
  }
  std::vector<ISmoluhovskiKoreFunc*> basis;
  basis.push_back(new KoreKG());
  basis.push_back(new KoreKB());

  std::vector<IUnaryFunc*> basis1;
  basis1.push_back(new C());
 // basis1.push_back(new Fx(1));
 // basis1.push_back(new Fx(2));
  //basis1.push_back(new Fx(3)); 

  std::vector<IUnaryFunc*> basis2;
  basis1.push_back(new C());
  //basis2.push_back(new Fx(1));
 // basis2.push_back(new Fx(2));
 // basis2.push_back(new Fx(3));
  //basis2.push_back(new Fx(4));

  //basis1.clear();
  //basis2.clear();

  RunCoreElevationBackTest(&nv, &gi, basis, basis1, basis2, new XiQSSASolver(), "REAL_BT.bmp");

  return;


  basis.clear();
  basis.push_back(new KoreY());
  basis.push_back(new KoreX());
 // RunCoreElevationBackTest(&nv, &gi, basis, basis1, basis2, new XiSimpleSolver(), "X_Y_BT.bmp");

  basis.clear();
  basis.push_back(new KoreY());
  basis.push_back(new KoreX());
  basis.push_back(new KoreExp_m_X2_Y2());
  RunCoreElevationBackTest(&nv, &gi, basis, basis1, basis2, new XiSimpleSolver(), "X_Y_EXP_X2_Y2.bmp");

  basis.clear();
  basis.push_back(new KoreSQRTX());
  basis.push_back(new KoreSQRTY());
  basis.push_back(new KoreExp_m_X2_Y2());
  RunCoreElevationBackTest(&nv, &gi, basis, basis1, basis2, new XiSimpleSolver(), "SQRTX_SQRTY_EXP_X2_Y2.bmp");

  /*

  basis.clear();
  basis.push_back(new KoreX());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "X.bmp");

  basis.clear();
  basis.push_back(new KoreY());
  basis.push_back(new KoreX());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "X_Y.bmp");

  basis.clear();
  basis.push_back(new KoreY());
  basis.push_back(new KoreX());
  basis.push_back(new KoreXY());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "X_Y_XY.bmp");

  basis.clear();
  basis.push_back(new KoreY2());
  basis.push_back(new KoreX2());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "X2_Y2.bmp");

  basis.clear();
  basis.push_back(new KoreY2());
  basis.push_back(new KoreX2());
  basis.push_back(new KoreXY());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "X2_Y2_XY.bmp");

  basis.clear();
  basis.push_back(new KoreY());
  basis.push_back(new KoreX());
  basis.push_back(new KoreExpX_Y());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "X_Y_EXPX_Y.bmp");


  basis.clear();
  basis.push_back(new KoreY());
  basis.push_back(new KoreX());
  basis.push_back(new KoreExp_m_X2_Y2());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "X_Y_EXP_X2_Y2.bmp");

  basis.clear();
  basis.push_back(new KoreSQRTX());
  basis.push_back(new KoreSQRTY());
  basis.push_back(new KoreExp_m_X2_Y2());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "SQRTX_SQRTY_EXP_X2_Y2.bmp");

  basis.clear();
  basis.push_back(new KoreSQRTX_Y());
  basis.push_back(new KoreExp_m_X2_Y2());
  RunCoreElevationBackTest(&nv, &gi, basis, new XiSimpleSolver(), "SQRTX_Y_EXP_X2_Y2.bmp");
  */
}