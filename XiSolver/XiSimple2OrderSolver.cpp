#include "XiSimple2OrderSolver.h"
#include <math.h>

void XiSimple2OrderSolver::MakeIteration(double* ipSourceValue, double* ipTargetValue)
{
  unsigned int n = _pInput->RadiusCount;
  double* pDestruction0 = new double[n];
  double* pProduction0 = new double[n];
  EvaluateProduction(ipSourceValue, pProduction0);
  EvaluateDestruction(ipSourceValue, pDestruction0);

  XiSimpleSolver::MakeIteration(ipSourceValue, ipTargetValue);
  double* pDestructionT = new double[n];
  double* pProductionT = new double[n];
  EvaluateProduction(ipTargetValue, pProductionT);
  EvaluateDestruction(ipTargetValue, pDestructionT);

  for(unsigned int i = 0; i < n; i++) {
    double psiIntegral0 = (pDestruction0[i] + pDestructionT[i]) * _pInput->TimeStep / 2;
    psiIntegral0 = exp(-psiIntegral0);
    double v1 = ipSourceValue[i] * psiIntegral0;
    double v2 = (pProduction0[i] * psiIntegral0 + pProductionT[i]) * _pInput->TimeStep / 2;
    ipTargetValue[i] = v1 + v2;
  }
  delete pDestruction0;
  delete pProduction0;
  delete pDestructionT;
  delete pProductionT;
}
