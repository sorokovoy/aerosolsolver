#include "XiSolverSmoluhovskiExactSolutions.h"
#include "GeneralCoreEquation.h"
#include <stdio.h>
#include <math.h>

#define Pi 3.14159265358979323846
#define ASMax(a,b)    (((a) > (b)) ? (a) : (b))
#define ASMin(a,b)    (((a) < (b)) ? (a) : (b))
#define F1 3200000000000      //{3.2*10^12/N} Source at the begining moment (amount of particles)
#define F2 0      //Sink at the begining moment 
//#define d 1  //[m] Diametre of a gas's molecule
#define Deff 1000 //[Kg/m^3] Effective density
#define Ox 0.0000000182 //[Pa*sec] Viscosity of oxegen
#define Pi 3.14159265358979323846
#define g 9.8 //[m/sec^2]
#define T 298 //[K] Temperature
#define K 0.0000000000000000000000138 // Bolzman's constant 
#define RGas 8.3144
#define dRk1 0.0000001 //meters
#define Tmax 3600 //[sec]
#define Ad 200 //[m^2] S of horizontal 
#define Ah 600 //[m^2] S ov vertical
#define deltaD 0.0001//[m]
#define V 2000 //[m^3]
#define D 1.7 //[Kg/m^3] Density of particle 
//#define Rop 1.00E003
#define Rop D
#define M 0.1
#define lambda 6.86E-008
#define Mu 1.83E-006
#define deltaPi 1.00E-004
#define L 0.0000001;

double E(double Ri, double Rk) //koefficient of effective of couple
{
  return( (0.5 * Ri * Ri) / ((Ri + Rk) * (Ri + Rk)) );
}

double Kn(double Ri)
{
  return 0.0000001 / Ri ; //L=0.0000001
}
double Cn(double Ri)
{
  return(1 + 1.246 * Kn(Ri) + 0.42 * Kn(Ri) * exp(-(0.87) / (Kn(Ri))));
}
double Us(double Ri)
{
  return(2 * Deff * g * Ri * Ri * Cn(Ri) / (9 * Ox));
}
double Kg(double Ri, double Rk)
{
  double fabsValue = fabs(Us(Ri) - Us(Rk));
  return(Pi * E(Ri, Rk) * (Ri + Rk) * (Ri + Rk) * (fabsValue));
}

double B(double Ri)
{
  return(Cn(Ri) / (6 * Pi * Ox * Ri));
}
double Kb(double Ri, double Rk)
{
  return(4 * Pi * K * T * (Ri + Rk) * (B(Ri) + B(Rk)));
}
double alphaD(double Ri)
{
  return(K * T * B(Ri) * Ad / (deltaD * V));
}
double alphaS(double Ri)
{
  return((1.222222222222222222) * Pi * D * Ri * Ri * Ri * B(Ri) * Ah / V);
}

class DoubleValueKashe
{
public:
  DoubleValueKashe()
  {
    pData = 0;
  }

  double getValue(unsigned int idx, unsigned int jdx, unsigned int n)
  {
    if(pData == 0) {
      pData = new double* [n];
      for(unsigned int i = 0; i < n; i++) {
        pData[i] = new double[n];
        for(unsigned int j = 0; j < n; j++) {
          pData[i][j] = -1;
        }
      }
    }

    if(pData[idx][jdx] < 0 ) {
      pData[idx][jdx] = CreateValue(idx, jdx);
    }
    return pData[idx][jdx];
  }

  virtual double CreateValue(unsigned int idx, unsigned int jdx) = 0;

protected:
  double** pData;
};

double dval = 0.0001;
double perVal = 0.0001;

class SmoluhovskiGeneralCoreIDestructionOperator : public IDestructionOperator, protected DoubleValueKashe
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < n; i++) {
     // res += ipArray[i] * (Kg(pMasterData->pRadius[i], pMasterData->pRadius[iCurrentIDx])+Kb(pMasterData->pRadius[i], pMasterData->pRadius[iCurrentIDx]));
      res += ipArray[i] * getValue(i, iCurrentIDx, n); // (Kg(pMasterData->pRadius[i], pMasterData->pRadius[iCurrentIDx])+Kb(pMasterData->pRadius[i], pMasterData->pRadius[iCurrentIDx]));
    }
    res+= alphaD(pMasterData->pRadius[iCurrentIDx])+alphaS(pMasterData->pRadius[iCurrentIDx]);
    res = res * pMasterData->RadiusStep;


    if(iCurrentIDx != n - 1 && iCurrentIDx != 0) {
      //res = res + (ipArray[iCurrentIDx + 1] - ipArray[iCurrentIDx]);// / pMasterData->RadiusStep;
      res = res + (ipArray[iCurrentIDx + 1]) * perVal;
      res = res + 2* ipArray[iCurrentIDx] * dval;
    }
    if(res < 0 ) {
       res = res;
    }

    return res;
  }

  virtual double CreateValue(unsigned int idx, unsigned int jdx)
  {
    return (Kg(pMasterData->pRadius[idx], pMasterData->pRadius[jdx])+Kb(pMasterData->pRadius[idx], pMasterData->pRadius[jdx]));
  }
};

class SmoluhovskiGeneralCoreIProductionOperator : public IProductionOperator, protected DoubleValueKashe
{
public:
  virtual double Evaluate(double* ipArray, unsigned int iCurrentIDx)
  {
    unsigned int n = pMasterData->RadiusCount;
    double res = 0;
    for(unsigned int i = 0; i < iCurrentIDx; i++) {
      res += ipArray[iCurrentIDx - i] * ipArray[i] * getValue(iCurrentIDx - i, i, n);  //(Kg(pMasterData->pRadius[iCurrentIDx - i], pMasterData->pRadius[i])+Kb(pMasterData->pRadius[iCurrentIDx - i], pMasterData->pRadius[i]));
    }
    res = res/2;
    res = res * pMasterData->RadiusStep;


    if(iCurrentIDx != n - 1 && iCurrentIDx != 0) {
      //res = res + (ipArray[iCurrentIDx - 1] - 2* ipArray[iCurrentIDx] + ipArray[iCurrentIDx + 1]);// / (pMasterData->RadiusStep * pMasterData->RadiusStep);
      res = res + (ipArray[iCurrentIDx - 1] + ipArray[iCurrentIDx + 1]) * dval;// / (pMasterData->RadiusStep * pMasterData->RadiusStep);
      res = res +  ipArray[iCurrentIDx] * perVal;
      if(res < 0 ) {
        res = res;
      }
    }

    return res;
  }

  virtual double CreateValue(unsigned int idx, unsigned int jdx)
  {
    return (Kg(pMasterData->pRadius[idx], pMasterData->pRadius[jdx])+Kb(pMasterData->pRadius[idx], pMasterData->pRadius[jdx]));
  }
};


XiSolverEqualationData* SmoluhovskiGeneralCore::CreateEqualationData()
{
  XiSolverEqualationData* pData = new XiSolverEqualationData();
  pData->pProduction = new SmoluhovskiGeneralCoreIProductionOperator();
  pData->pDestruction = new SmoluhovskiGeneralCoreIDestructionOperator();
  return pData;
}

double SmoluhovskiGeneralCore::Solution(double x, double t)
{
	return 0;
}

double SmoluhovskiGeneralCore::Boundary(double x)
{
	return 3.2E012;
}

