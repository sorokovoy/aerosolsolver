#ifndef XiQSSA2OrderSolver_H
#define XiQSSA2OrderSolver_H

#include "XiQSSASolver.h"

class XiQSSA2OrderSolver : public XiQSSASolver
{
protected:
  virtual void MakeIteration(double* ipSourceValue, double* ipTargetValue);
};

#endif