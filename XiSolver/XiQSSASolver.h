#ifndef XiQSSASolver_H
#define XiQSSASolver_H

#include "IXiSolver.h"

class XiQSSASolver : public IXiSolver
{
protected:
  virtual void MakeIteration(double* ipSourceValue, double* ipTargetValue);
};

#endif