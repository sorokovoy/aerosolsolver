#ifndef XIDensyImageGenerator_H
#define XIDensyImageGenerator_H

#include "CImg.h"
using namespace cimg_library;

class IXiSolver;
struct XiSolverEqualationData;

struct RGBColor
{
  unsigned char r;
  unsigned char g;
  unsigned char b;
};

int GetDistance(const RGBColor& iV1, const RGBColor& iV2);
RGBColor Get(const CImg<unsigned char>& iFrom, int x, int y);
void Set(CImg<unsigned char>& iFrom, int x, int y, const RGBColor& v);

class RGBColorGradientInterpretator
{
public:
  RGBColorGradientInterpretator(const char* ipGradFile, double iMinValue = 0, double iMaxValue = 1);
  double Recognize(const RGBColor& iV);
  RGBColor ToColor(double v);

private:
  CImg<unsigned char>* _pImage;
  double _minV;
  double _maxV;
};

struct XIDensyImageGeneratorSettings
{
  const char* pSourceGradFile;
  double gradMaxVal;
  double gradMinVal;

  const char* pOutFile;

  unsigned int nRadCount;
  double rad0;
  double radStep;

  double lineZeroVal;
  double lineValue;

  unsigned int timeStepCount;
  double timeStep;
};

class XIDensyImageGenerator
{
public:
  void Generate(XiSolverEqualationData* ipEqData, IXiSolver*  ipSolver, const XIDensyImageGeneratorSettings& iSettings);
};

#endif 